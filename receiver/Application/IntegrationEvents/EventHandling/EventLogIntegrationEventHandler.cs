using System;
using System.Threading.Tasks;
using Library.EventBus.Abstractions;

namespace receiver.Application.IntegrationEvents.EventHandling
{
  public class EventLogIntegrationEventHandler : IIntegrationEventHandler<EventLogIntegrationEvent>
  {
    public EventLogIntegrationEventHandler()
    {
      
    }


    public async Task Handle(EventLogIntegrationEvent @event)
    {
      System.Console.WriteLine("Event Log Integration Event Handler");
    }
  }
}
