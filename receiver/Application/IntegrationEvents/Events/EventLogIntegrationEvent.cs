using Library.EventBus.Events;

namespace receiver.Application.IntegrationEvents
{
    public class EventLogIntegrationEvent : IntegrationEvent
    {
        public EventLogIntegrationEvent(string message)
        {
            Message = message;
        }
        public string Message { get; }
    }
}
