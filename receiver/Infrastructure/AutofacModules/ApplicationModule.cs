using Autofac;
using Library.EventBus.Abstractions;
using receiver.Application.IntegrationEvents;
using System.Reflection;
using Module = Autofac.Module;

namespace receiver.Infrastructure.AutofacModules
{
    public class ApplicationModule
        : Module
    {
        public ApplicationModule()
        {
        }


        protected override void Load(ContainerBuilder builder)
        {
          builder.RegisterAssemblyTypes(typeof(EventLogIntegrationEvent).GetTypeInfo()
                  .Assembly)
              .AsClosedTypesOf(typeof(IIntegrationEventHandler<>));
        }
    }
}
