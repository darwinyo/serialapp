using Autofac;
using Library.EventBus.Abstractions;
using sender.Application.IntegrationEvents;
using System.Reflection;
using Module = Autofac.Module;

namespace sender.Infrastructure.AutofacModules
{
    public class ApplicationModule
        : Module
    {
        public ApplicationModule()
        {
        }


        protected override void Load(ContainerBuilder builder)
        {
        }
    }
}
